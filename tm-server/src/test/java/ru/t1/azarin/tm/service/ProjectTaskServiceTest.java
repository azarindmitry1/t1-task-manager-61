package ru.t1.azarin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.api.service.dto.IProjectDtoService;
import ru.t1.azarin.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.azarin.tm.api.service.dto.ITaskDtoService;
import ru.t1.azarin.tm.api.service.dto.IUserDtoService;
import ru.t1.azarin.tm.configuration.ServerConfiguration;
import ru.t1.azarin.tm.dto.model.TaskDto;
import ru.t1.azarin.tm.dto.model.UserDto;
import ru.t1.azarin.tm.exception.field.ProjectIdEmptyException;
import ru.t1.azarin.tm.exception.field.TaskIdEmptyException;
import ru.t1.azarin.tm.exception.field.UserIdEmptyException;
import ru.t1.azarin.tm.marker.UnitCategory;
import ru.t1.azarin.tm.util.HashUtil;

import static ru.t1.azarin.tm.constant.ProjectTestData.USER1_PROJECT1;
import static ru.t1.azarin.tm.constant.ProjectTestData.USER1_PROJECT2;
import static ru.t1.azarin.tm.constant.TaskTestData.USER1_TASK1;
import static ru.t1.azarin.tm.constant.TaskTestData.USER1_TASK2;
import static ru.t1.azarin.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class ProjectTaskServiceTest {

    @NotNull
    @Autowired
    private static IPropertyService PROPERTY_SERVICE;

    @NotNull
    @Autowired
    private static IProjectDtoService PROJECT_SERVICE;

    @NotNull
    @Autowired
    private static ITaskDtoService TASK_SERVICE;

    @NotNull
    @Autowired
    private static IProjectTaskDtoService PROJECT_TASK_SERVICE;

    @NotNull
    @Autowired
    private static IUserDtoService USER_SERVICE;

    @NotNull
    private static String USER_ID = "";

    @NotNull
    private final String emptyString = "";

    @Nullable
    private final String nullString = null;

    @BeforeClass
    public static void setUp() {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        PROPERTY_SERVICE = context.getBean(IPropertyService.class);
        PROJECT_SERVICE = context.getBean(IProjectDtoService.class);
        TASK_SERVICE = context.getBean(ITaskDtoService.class);
        PROJECT_TASK_SERVICE = context.getBean(IProjectTaskDtoService.class);
        USER_SERVICE = context.getBean(IUserDtoService.class);
        @NotNull final UserDto user = new UserDto();
        user.setLogin(USER1_LOGIN);
        user.setPasswordHash(HashUtil.salt(PROPERTY_SERVICE, USER1_PASSWORD));
        user.setEmail(USER1_EMAIL);
        USER_SERVICE.add(user);
        USER_ID = user.getId();
    }

    @AfterClass
    public static void tearDown() {
        @Nullable final UserDto user = USER_SERVICE.findByLogin(USER1_LOGIN);
        USER_SERVICE.removeByLogin(user.getLogin());
    }

    @Before
    public void before() {
        USER1_PROJECT1.setUserId(USER_ID);
        USER1_PROJECT2.setUserId(USER_ID);
        USER1_TASK1.setUserId(USER_ID);
        USER1_TASK2.setUserId(USER_ID);
        PROJECT_SERVICE.add(USER1_PROJECT1);
        PROJECT_SERVICE.add(USER1_PROJECT2);
        TASK_SERVICE.add(USER1_TASK1);
        TASK_SERVICE.add(USER1_TASK2);
    }

    @After
    public void after() {
        TASK_SERVICE.clear(USER_ID);
        PROJECT_SERVICE.clear(USER_ID);
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_TASK_SERVICE.bindTaskToProject(
                emptyString, USER1_PROJECT1.getId(), USER1_TASK1.getId())
        );
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_TASK_SERVICE.bindTaskToProject(
                nullString, USER1_PROJECT1.getId(), USER1_TASK1.getId())
        );
        Assert.assertThrows(ProjectIdEmptyException.class, () -> PROJECT_TASK_SERVICE.bindTaskToProject(
                USER1.getId(), emptyString, USER1_TASK1.getId())
        );
        Assert.assertThrows(ProjectIdEmptyException.class, () -> PROJECT_TASK_SERVICE.bindTaskToProject(
                USER1.getId(), nullString, USER1_TASK1.getId())
        );
        Assert.assertThrows(TaskIdEmptyException.class, () -> PROJECT_TASK_SERVICE.bindTaskToProject(
                USER1.getId(), USER1_PROJECT1.getId(), emptyString)
        );
        Assert.assertThrows(TaskIdEmptyException.class, () -> PROJECT_TASK_SERVICE.bindTaskToProject(
                USER1.getId(), USER1_PROJECT1.getId(), nullString)
        );
        PROJECT_TASK_SERVICE.bindTaskToProject(USER_ID, USER1_PROJECT1.getId(), USER1_TASK1.getId());
        @Nullable final TaskDto task = TASK_SERVICE.findOneById(USER_ID, USER1_TASK1.getId());
        Assert.assertEquals(USER1_PROJECT1.getId(), task.getProjectId());
    }

    @Test
    public void unbindTaskToProject() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_TASK_SERVICE.unbindTaskToProject(
                emptyString, USER1_PROJECT1.getId(), USER1_TASK1.getId())
        );
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_TASK_SERVICE.unbindTaskToProject(
                nullString, USER1_PROJECT1.getId(), USER1_TASK1.getId())
        );
        Assert.assertThrows(ProjectIdEmptyException.class, () -> PROJECT_TASK_SERVICE.unbindTaskToProject(
                USER_ID, emptyString, USER1_TASK1.getId())
        );
        Assert.assertThrows(ProjectIdEmptyException.class, () -> PROJECT_TASK_SERVICE.unbindTaskToProject(
                USER_ID, nullString, USER1_TASK1.getId())
        );
        Assert.assertThrows(TaskIdEmptyException.class, () -> PROJECT_TASK_SERVICE.unbindTaskToProject(
                USER_ID, USER1_PROJECT1.getId(), emptyString)
        );
        Assert.assertThrows(TaskIdEmptyException.class, () -> PROJECT_TASK_SERVICE.unbindTaskToProject(
                USER_ID, USER1_PROJECT1.getId(), nullString)
        );
        PROJECT_TASK_SERVICE.unbindTaskToProject(USER_ID, USER1_PROJECT1.getId(), USER1_TASK1.getId());
        Assert.assertNull(USER1_TASK1.getProjectId());
    }

    @Test
    public void removeProjectById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_TASK_SERVICE.removeProjectById(
                emptyString, USER1_PROJECT1.getId())
        );
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_TASK_SERVICE.removeProjectById(
                nullString, USER1_PROJECT1.getId())
        );
        Assert.assertThrows(ProjectIdEmptyException.class, () -> PROJECT_TASK_SERVICE.removeProjectById(
                USER_ID, emptyString)
        );
        Assert.assertThrows(ProjectIdEmptyException.class, () -> PROJECT_TASK_SERVICE.removeProjectById(
                USER_ID, nullString)
        );
        PROJECT_TASK_SERVICE.removeProjectById(USER_ID, USER1_PROJECT2.getId());
        Assert.assertNull(PROJECT_SERVICE.findOneById(USER_ID, USER1_PROJECT2.getId()));
    }

}
