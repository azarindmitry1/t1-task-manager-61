package ru.t1.azarin.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.dto.model.TaskDto;
import ru.t1.azarin.tm.enumerated.Status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@UtilityClass
public final class TaskTestData {

    @NotNull
    public final static TaskDto USER1_TASK1 = new TaskDto();

    @NotNull
    public final static TaskDto USER1_TASK2 = new TaskDto();

    @NotNull
    public final static TaskDto ADMIN_TASK1 = new TaskDto();

    @NotNull
    public final static TaskDto ADMIN_TASK2 = new TaskDto();

    @NotNull
    public final static List<TaskDto> USER1_TASK_LIST = Arrays.asList(USER1_TASK1, USER1_TASK2);

    @NotNull
    public final static List<TaskDto> ADMIN_TASK_LIST = Arrays.asList(ADMIN_TASK1, ADMIN_TASK2);

    @NotNull
    public final static List<TaskDto> TASK_LIST = new ArrayList<>();

    static {
        TASK_LIST.addAll(USER1_TASK_LIST);
        TASK_LIST.addAll(ADMIN_TASK_LIST);

        for (int i = 0; i < TASK_LIST.size(); i++) {
            @NotNull final TaskDto task = TASK_LIST.get(i);
            task.setName("task-" + i);
            task.setDescription("description-" + i);
            task.setStatus(Status.NOT_STARTED);
        }
    }

}
