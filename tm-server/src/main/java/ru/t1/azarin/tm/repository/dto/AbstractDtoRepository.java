package ru.t1.azarin.tm.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.azarin.tm.dto.model.AbstractDtoModel;

@Repository
public interface AbstractDtoRepository<M extends AbstractDtoModel> extends JpaRepository<M, String> {

}
