package ru.t1.azarin.tm.dto.response.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.model.ProjectDto;
import ru.t1.azarin.tm.dto.response.AbstractResponse;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectListResponse extends AbstractResponse {

    @Nullable
    private List<ProjectDto> projects;

}
