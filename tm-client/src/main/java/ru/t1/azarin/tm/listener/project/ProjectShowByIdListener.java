package ru.t1.azarin.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.azarin.tm.dto.model.ProjectDto;
import ru.t1.azarin.tm.dto.request.project.ProjectShowByIdRequest;
import ru.t1.azarin.tm.event.ConsoleEvent;
import ru.t1.azarin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.azarin.tm.util.TerminalUtil;

@Component
public final class ProjectShowByIdListener extends AbstractProjectListener {

    @NotNull
    public final static String NAME = "project-show-by-id";

    @NotNull
    public final static String DESCRIPTION = "Show project by id.";

    @Override
    @EventListener(condition = "@projectShowByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[FIND PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(getToken());
        request.setId(id);
        @NotNull final ProjectDto project = projectEndpoint.showByIdResponse(request).getProject();
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
