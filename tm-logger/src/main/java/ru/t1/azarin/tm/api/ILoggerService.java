package ru.t1.azarin.tm.api;

import org.jetbrains.annotations.NotNull;

public interface ILoggerService {

    void log(@NotNull String json);

}
